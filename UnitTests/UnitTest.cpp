/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/ParenPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::ParenPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::ParenPrinter >( tmp, argc, argv );
    return status;
}
bool suite_ControllerManagerTestSuite_init = false;
#include "UnitTest.h"

static ControllerManagerTestSuite suite_ControllerManagerTestSuite;

static CxxTest::List Tests_ControllerManagerTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_ControllerManagerTestSuite( "UnitTest.h", 20, "ControllerManagerTestSuite", suite_ControllerManagerTestSuite, Tests_ControllerManagerTestSuite );

static class TestDescription_suite_ControllerManagerTestSuite_testAt_least_1_controller_is_present : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControllerManagerTestSuite_testAt_least_1_controller_is_present() : CxxTest::RealTestDescription( Tests_ControllerManagerTestSuite, suiteDescription_ControllerManagerTestSuite, 38, "testAt_least_1_controller_is_present" ) {}
 void runTest() { suite_ControllerManagerTestSuite.testAt_least_1_controller_is_present(); }
} testDescription_suite_ControllerManagerTestSuite_testAt_least_1_controller_is_present;

static class TestDescription_suite_ControllerManagerTestSuite_testEvents_reach_controller : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControllerManagerTestSuite_testEvents_reach_controller() : CxxTest::RealTestDescription( Tests_ControllerManagerTestSuite, suiteDescription_ControllerManagerTestSuite, 42, "testEvents_reach_controller" ) {}
 void runTest() { suite_ControllerManagerTestSuite.testEvents_reach_controller(); }
} testDescription_suite_ControllerManagerTestSuite_testEvents_reach_controller;

static ControlDeviceTestSuite suite_ControlDeviceTestSuite;

static CxxTest::List Tests_ControlDeviceTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_ControlDeviceTestSuite( "UnitTest.h", 52, "ControlDeviceTestSuite", suite_ControlDeviceTestSuite, Tests_ControlDeviceTestSuite );

static class TestDescription_suite_ControlDeviceTestSuite_testKeypress_is_not_registered : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testKeypress_is_not_registered() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 65, "testKeypress_is_not_registered" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testKeypress_is_not_registered(); }
} testDescription_suite_ControlDeviceTestSuite_testKeypress_is_not_registered;

static class TestDescription_suite_ControlDeviceTestSuite_testInvalidKeypress_returns_false : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testInvalidKeypress_returns_false() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 72, "testInvalidKeypress_returns_false" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testInvalidKeypress_returns_false(); }
} testDescription_suite_ControlDeviceTestSuite_testInvalidKeypress_returns_false;

static class TestDescription_suite_ControlDeviceTestSuite_testStickmovement_is_not_registered : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testStickmovement_is_not_registered() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 76, "testStickmovement_is_not_registered" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testStickmovement_is_not_registered(); }
} testDescription_suite_ControlDeviceTestSuite_testStickmovement_is_not_registered;

static class TestDescription_suite_ControlDeviceTestSuite_testInvalidStickmovement_returns_false : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testInvalidStickmovement_returns_false() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 82, "testInvalidStickmovement_returns_false" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testInvalidStickmovement_returns_false(); }
} testDescription_suite_ControlDeviceTestSuite_testInvalidStickmovement_returns_false;

static class TestDescription_suite_ControlDeviceTestSuite_testKeypress_is_registered : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testKeypress_is_registered() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 86, "testKeypress_is_registered" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testKeypress_is_registered(); }
} testDescription_suite_ControlDeviceTestSuite_testKeypress_is_registered;

static class TestDescription_suite_ControlDeviceTestSuite_testInvalidKeypress_is_not_registered : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testInvalidKeypress_is_not_registered() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 92, "testInvalidKeypress_is_not_registered" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testInvalidKeypress_is_not_registered(); }
} testDescription_suite_ControlDeviceTestSuite_testInvalidKeypress_is_not_registered;

static class TestDescription_suite_ControlDeviceTestSuite_testStickPos_is_registered : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testStickPos_is_registered() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 98, "testStickPos_is_registered" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testStickPos_is_registered(); }
} testDescription_suite_ControlDeviceTestSuite_testStickPos_is_registered;

static class TestDescription_suite_ControlDeviceTestSuite_testKey_state_is_reset_when_key__is_depressed : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testKey_state_is_reset_when_key__is_depressed() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 114, "testKey_state_is_reset_when_key__is_depressed" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testKey_state_is_reset_when_key__is_depressed(); }
} testDescription_suite_ControlDeviceTestSuite_testKey_state_is_reset_when_key__is_depressed;

static class TestDescription_suite_ControlDeviceTestSuite_testScancode_activates_correct_button : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testScancode_activates_correct_button() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 121, "testScancode_activates_correct_button" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testScancode_activates_correct_button(); }
} testDescription_suite_ControlDeviceTestSuite_testScancode_activates_correct_button;

static class TestDescription_suite_ControlDeviceTestSuite_testScancode_activates_correct_stick : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testScancode_activates_correct_stick() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 132, "testScancode_activates_correct_stick" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testScancode_activates_correct_stick(); }
} testDescription_suite_ControlDeviceTestSuite_testScancode_activates_correct_stick;

static class TestDescription_suite_ControlDeviceTestSuite_testRemapping_button_still_registers_keypresses_correctly : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testRemapping_button_still_registers_keypresses_correctly() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 145, "testRemapping_button_still_registers_keypresses_correctly" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testRemapping_button_still_registers_keypresses_correctly(); }
} testDescription_suite_ControlDeviceTestSuite_testRemapping_button_still_registers_keypresses_correctly;

static class TestDescription_suite_ControlDeviceTestSuite_testRemapping_button_still_registers_stick_updates_correctly : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ControlDeviceTestSuite_testRemapping_button_still_registers_stick_updates_correctly() : CxxTest::RealTestDescription( Tests_ControlDeviceTestSuite, suiteDescription_ControlDeviceTestSuite, 153, "testRemapping_button_still_registers_stick_updates_correctly" ) {}
 void runTest() { suite_ControlDeviceTestSuite.testRemapping_button_still_registers_stick_updates_correctly(); }
} testDescription_suite_ControlDeviceTestSuite_testRemapping_button_still_registers_stick_updates_correctly;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
