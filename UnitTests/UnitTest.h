#pragma once
// Copyright(c) 2016, Rodolfo Lam
// All rights reserved.
// See the LICENSE file located in the main directory for
// the details.
#include "cxxtest/TestSuite.h"
#include <allegro5\allegro.h>
#include "../ControllerSys/ControllerManager.h"

/*
  See https://bitbucket.org/rlam1/game/wiki/Regenerating%20the%20test%20suites%20in%20Visual%20Studio
  for updated information, but it boils down to:

    1. Delete UnitTest.cpp from the project and from the folder.
    2. Build ONLY UnitTests project
    3. Add to source files the newly created UnitTest.cpp
    4. Build again the project
    5. Run as normal the solution.
*/

class ControllerManagerTestSuite : public CxxTest::TestSuite {
    ControllerManager *mngr;
    /* TODO: Define a mock for al_get_num_joysticks()
        this way we can test without a joystick present at the system.*/
public:
    void setUp() {
        assert(al_init());
        assert(al_install_keyboard());
        assert(al_install_joystick());
        mngr = new ControllerManager();
    }

    void tearDown() {
        al_uninstall_system();
        delete mngr;
    }

    void testAt_least_1_controller_is_present() {
        TS_ASSERT((mngr->GetNumOfControllers() > 0));
    }

    void testEvents_reach_controller() {
        mngr->UpdateDeviceKeyboard(11, true); //ALLEGRO_KEY_K is 11
        TS_ASSERT_EQUALS(true, mngr->GetButton(buttonID::button_A));

        mngr->UpdateDeviceKeyboard(11, false);
        TS_ASSERT_EQUALS(false, mngr->GetButton(buttonID::button_A));

        /*TODO: TEST THE ANALOG AND DIGITAL CODE PATHS. NEED MOCK SEE NOTE ABOVE.*/
    }

    void testJoystick_events_only_update_intended_joysticks() {
        if(mngr->GetNumOfControllers() > 1)
        {
            mngr->UpdateDeviceAnalog((int)stickID::stick_R, 0, 0.5);
            mngr->UpdateDeviceAnalog((int)stickID::stick_R, 1, 0.5);

            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_L));
        } else
        {
            TS_SKIP("Plug a controller to test this.");
        }
    }

    void testLeftJoy_is_not_updated_by_other_joysticks() {
        if(mngr->GetNumOfControllers() > 1)
        {
            mngr->UpdateDeviceAnalog((int)stickID::stick_R, 0, 0.5);
            mngr->UpdateDeviceAnalog((int)stickID::stick_R, 1, 0.5);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_L));

            mngr->UpdateDeviceAnalog((int)stickID::stick_L2, 0, 0.5);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_L));

            mngr->UpdateDeviceAnalog((int)stickID::stick_R2, 0, 0.5);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_L));

        } else
        {
            TS_SKIP("Plug a controller to test this.");
        }
    }

    void testRightJoy_is_not_updated_by_other_joysticks() {
        if(mngr->GetNumOfControllers() > 1)
        {
            mngr->UpdateDeviceAnalog((int)stickID::stick_L, 0, 0.1);
            mngr->UpdateDeviceAnalog((int)stickID::stick_L, 1, 0.2);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_R));

            mngr->UpdateDeviceAnalog((int)stickID::stick_L2, 0, 0.3);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_R));

            mngr->UpdateDeviceAnalog((int)stickID::stick_R2, 0, 0.4);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_R));

        } else
        {
            TS_SKIP("Plug a controller to test this.");
        }
    }

    void testL2_is_not_updated_by_other_joysticks() {
        if(mngr->GetNumOfControllers() > 1)
        {
            mngr->UpdateDeviceAnalog((int)stickID::stick_L, 0, 0.1);
            mngr->UpdateDeviceAnalog((int)stickID::stick_L, 1, 0.2);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_L2));

            mngr->UpdateDeviceAnalog((int)stickID::stick_R, 0, 0.3);
            mngr->UpdateDeviceAnalog((int)stickID::stick_R, 1, 0.4);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_L2));

            mngr->UpdateDeviceAnalog((int)stickID::stick_R2, 0, 0.5);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_L2));

        } else
        {
            TS_SKIP("Plug a controller to test this.");
        }
    }

    void testR2_is_not_updated_by_other_joysticks() {
        if(mngr->GetNumOfControllers() > 1)
        {
            mngr->UpdateDeviceAnalog((int)stickID::stick_L, 0, 0.1);
            mngr->UpdateDeviceAnalog((int)stickID::stick_L, 1, 0.2);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_R2));

            mngr->UpdateDeviceAnalog((int)stickID::stick_R, 0, 0.3);
            mngr->UpdateDeviceAnalog((int)stickID::stick_R, 1, 0.4);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_R2));

            mngr->UpdateDeviceAnalog((int)stickID::stick_L2, 0, 0.5);
            TS_ASSERT_EQUALS(Vec2D(), mngr->GetAxis(stickID::stick_R2));

        } else
        {
            TS_SKIP("Plug a controller to test this.");
        }
    }
};

class ControlDeviceTestSuite : public CxxTest::TestSuite {
    ControlDevice *device;

public:
    void setUp() {
        device = new ControlDevice();
    }

    void tearDown() {
        delete device;
    }

    void testKeypress_is_not_registered() {
        TS_ASSERT(!device->GetButtonValue(buttonID::button_A));
        TS_ASSERT(!device->GetButtonValue(buttonID::button_Up));
        TS_ASSERT(!device->GetButtonValue(buttonID::button_Select));
        TS_ASSERT(!device->GetButtonValue(buttonID::button_R3));
    }

    void testInvalidKeypress_returns_false() {
        TS_ASSERT_EQUALS(false, device->GetButtonValue((buttonID)INT_MAX));
    }

    void testStickmovement_is_not_registered() {
        Vec2D pos = device->GetStickPos(stickID::stick_L2);
        TS_ASSERT_EQUALS(0.0, pos.x);
        TS_ASSERT_EQUALS(0.0, pos.y);
    }

    void testInvalidStickmovement_returns_false() {
        TS_ASSERT_EQUALS(Vec2D(0.0, 0.0), device->GetStickPos((stickID)INT_MAX));
    }

    void testKeypress_is_registered() {
        device->updateButton(buttonID::button_A, true);

        TS_ASSERT_EQUALS(true, device->GetButtonValue(buttonID::button_A));
    }

    void testInvalidKeypress_is_not_registered() {
        device->updateButton((buttonID)INT_MAX, true);

        TS_ASSERT_EQUALS(false, device->GetButtonValue((buttonID)INT_MAX));
    }

    void testStickPos_is_registered() {
        device->updateStick(stickID::stick_L, 0, 0.9);
        device->updateStick(stickID::stick_L, 1, -0.5);

        TS_ASSERT_EQUALS(Vec2D(0.9, -0.5), device->GetStickPos(stickID::stick_L));

        device->updateStick(stickID::stick_R, 0, -1.0);
        device->updateStick(stickID::stick_R, 1, 0.4);

        TS_ASSERT_EQUALS(Vec2D(-1.0, 0.4), device->GetStickPos(stickID::stick_R));

        device->updateStick(stickID::stick_L2, 0, 0.8);

        TS_ASSERT_EQUALS(Vec2D(0.8, 0.0), device->GetStickPos(stickID::stick_L2));
    }

    void testKey_state_is_reset_when_key__is_depressed() {
        device->updateButton(buttonID::button_A, true);
        device->updateButton(buttonID::button_A, false);

        TS_ASSERT_EQUALS(false, device->GetButtonValue(buttonID::button_A));
    }

    void testScancode_activates_correct_button() {
        device->mapButtonToScanCode(buttonID::button_A, 13);
        device->updateButton(13, true);

        TS_ASSERT_EQUALS(true, device->GetButtonValue(buttonID::button_A));

        device->updateButton(13, false);

        TS_ASSERT_EQUALS(false, device->GetButtonValue(buttonID::button_A));
    }

    void testScancode_activates_correct_stick() {
        device->mapStickToScanCode(stickID::stick_L, 20);
        device->updateStick(20, 0, 1.0);
        device->updateStick(20, 1, 0.5);

        TS_ASSERT_EQUALS(Vec2D(1.0, 0.5), device->GetStickPos(stickID::stick_L));

        device->mapStickToScanCode(stickID::stick_R2, 21);
        device->updateStick(21, 2, -0.5);

        TS_ASSERT_EQUALS(Vec2D(-0.5, 0.0), device->GetStickPos(stickID::stick_R2));
    }

    void testRemapping_button_still_registers_keypresses_correctly() {
        device->mapButtonToScanCode(buttonID::button_A, 13);
        device->updateButton(13, true);
        device->mapButtonToScanCode(buttonID::button_A, 15);

        TS_ASSERT_EQUALS(true, device->GetButtonValue(buttonID::button_A));
    }

    void testRemapping_button_still_registers_stick_updates_correctly() {
        device->mapStickToScanCode(stickID::stick_L, 20);
        device->updateStick(20, 0, 1.0);
        device->updateStick(20, 1, 0.5);
        device->mapStickToScanCode(stickID::stick_L, 21);

        TS_ASSERT_EQUALS(Vec2D(1.0, 0.5), device->GetStickPos(stickID::stick_L));
    }
};
