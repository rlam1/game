// Copyright(c) 2016, Rodolfo Lam
// All rights reserved.
// See the LICENSE file located in the main directory for
// the details.

#include "stdafx.h"
#include "ControllerSys\ControllerManager.h"

ALLEGRO_DISPLAY     *display  = nullptr;
ALLEGRO_EVENT_QUEUE *queue    = nullptr;
ALLEGRO_TIMER       *timer    = nullptr;
ALLEGRO_FONT        *textFont = nullptr;

bool init();
void outputData(const ControllerManager &manager);
void updateTestPlayer(const ControllerManager &manager, Vec2D &pos);

int main() {
    if(!init())
    {
        std::cout << "FATAL ERROR, Exiting..." << std::endl;
        return -1;
    }

    bool done = false;
    bool redraw = false;

    ControllerManager playerInputs;
    Vec2D testPos;

    al_start_timer(timer);
    while(!done)
    {
        ALLEGRO_EVENT ev;
        al_wait_for_event(queue, &ev);

        switch(ev.type)
        {
            case ALLEGRO_EVENT_TIMER:
                updateTestPlayer(playerInputs, testPos);
                redraw = true;
                break;
            case ALLEGRO_EVENT_JOYSTICK_AXIS:
                playerInputs.UpdateDeviceAnalog(ev.joystick.stick, ev.joystick.axis, ev.joystick.pos);
                break;
            case ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN:
                playerInputs.UpdateDeviceDigital(ev.joystick.button, true);
                break;
            case ALLEGRO_EVENT_JOYSTICK_BUTTON_UP:
                playerInputs.UpdateDeviceDigital(ev.joystick.button, false);
                break;
            case ALLEGRO_EVENT_JOYSTICK_CONFIGURATION:
                playerInputs.UpdateDeviceList();
                break;
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                done = true;
                break;
            case ALLEGRO_EVENT_KEY_DOWN:
                playerInputs.UpdateDeviceKeyboard(ev.keyboard.keycode, true);

                if(ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
                {
                    done = true;
                } else if(ev.keyboard.keycode == ALLEGRO_KEY_TAB)
                {
                    outputData(playerInputs);
                }
                break;
            case ALLEGRO_EVENT_KEY_UP:
                playerInputs.UpdateDeviceKeyboard(ev.keyboard.keycode, false);
                break;
        }

        if(redraw && al_is_event_queue_empty(queue))
        {
            redraw = false;

            al_clear_to_color(al_map_rgb(0, 0, 0));

            al_draw_filled_rectangle(testPos.x, testPos.y, testPos.x + 50.0f, testPos.y + 50.0f, al_map_rgb(255, 0, 255));


            al_flip_display();
        }
    }

    return 0;
}

bool init() {
    al_init();

    al_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_REQUIRE);
    display = al_create_display(1280, 720);
    timer = al_create_timer(1.0 / 60.0);
    queue = al_create_event_queue();
    textFont = al_create_builtin_font();

    if((display == nullptr) || (timer == nullptr) || (queue == nullptr) || (textFont == nullptr))
    {
        std::cout << "ERROR: Either the display, timer, queue or font failed to initialize!" << std::endl
            << "0x" << display << " 0x" << timer << " 0x" << queue << " 0x" << textFont << std::endl;
        return false;
    }

    al_install_keyboard();
    al_install_mouse();
    al_install_joystick();
    al_init_primitives_addon();
    al_init_native_dialog_addon();
    al_init_image_addon();
    al_init_font_addon();
    al_init_ttf_addon();

    al_register_event_source(queue, al_get_keyboard_event_source());
    al_register_event_source(queue, al_get_mouse_event_source());
    al_register_event_source(queue, al_get_joystick_event_source());
    al_register_event_source(queue, al_get_display_event_source(display));
    al_register_event_source(queue, al_get_timer_event_source(timer));

    return true;
}

void outputData(const ControllerManager &manager) {
    std::cout << "*** INPUT READOUT ***" << std::endl
        << "Controllers Detected: " << manager.GetNumOfControllers() << std::endl
        << "Digital Values: " << std::endl << "  ";

    for(int i = 0; i < 14; i++) // 14 is the number of buttons on a xbox controller
    {
        std::cout << manager.GetButton((buttonID)i) << " ";
    }
    std::cout << std::endl;

    std::cout << "Analog Values: " << std::endl << " ";
    for(int i = 0; i < 4; i++) // 4 is the number of sticks said controller has.
    {
        std::cout << " (" << manager.GetAxis((stickID)i).x << ","
            << manager.GetAxis((stickID)i).y << ") ";
    }

    std::cout << std::endl << std::endl;
}

void updateTestPlayer(const ControllerManager & manager, Vec2D & pos) {
    constexpr auto speed = 5.0f;
    constexpr auto deadzone = 0.1f;
    Vec2D stickpos = manager.GetAxis(stickID::stick_L);

    //Process joystick
    if(abs(stickpos.x) < deadzone)
    {
        stickpos.x = 0.0f;
    }
    if(abs(stickpos.y) < deadzone)
    {
        stickpos.y = 0.0f;
    }
    pos.x += stickpos.x * speed;
    pos.y += stickpos.y * speed;

    //Process keyboard arrows
    if(manager.GetButton(buttonID::button_Right))
    {
        pos.x += speed;
    }
    if(manager.GetButton(buttonID::button_Left))
    {
        pos.x -= speed;
    }
    if(manager.GetButton(buttonID::button_Down))
    {
        pos.y += speed;
    }
    if(manager.GetButton(buttonID::button_Up))
    {
        pos.y -= speed;
    }
}
