# Game, a TDD Experiment #

Game is an Allegro 5 Project that will be fully developed using Test Driven Development methodology. The Unit Testing Framework currently used is [CxxTest](http://cxxtest.com/).

### Details ###

* Will eventually become a fully functional 2D game. Haven't decided yet what will it be about.
* TDD *should* make it robust and minimize the amount of bugs that escape debugging.
* At the moment it is just a blank screen controller input receiver (does nothing with it)
* Updated whenever I have free time between weeks.

### How do I get set up? ###

* The Project currently supports Visual Studio 2015 **only**.

    The code itself should not be hard to port to GCC or Clang. It is just that no CMake or Premake files have been made for it. In the future this might change.

* The Visual Studio solution is already configured to run the code if downloaded from here and opened at the editor.

    Special instructions to generate and compile the Unit Tests are found on the [wiki](https://bitbucket.org/rlam1/game/wiki/Regenerating%20the%20test%20suites%20in%20Visual%20Studio)

* Current dependencies:
    
    * Allegro 5.1 from NuGet.
    * The CxxTest testing framework plugged into the Project. (Should be configured on the Solution).


* Running Tests:

    The Test Project produces an executable that contains all Unit Tests. This can be run outside the IDE to verify tests pass. Additionally, whenever you compile the full code base, tests are executed and the results are routed to the compilation console window. Compiling only the Test Project will cause the same results.

### Contribution guidelines ### **TODO**

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ### **TODO**

* Repo owner or admin
* Other community or team contact