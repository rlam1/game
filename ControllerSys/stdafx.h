// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <map>
#include <array>

// Allegro 5 partial API
#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#include <Game/Vec2D.h>

// TODO: reference additional headers your program requires here
