#pragma once
// Copyright(c) 2016, Rodolfo Lam
// All rights reserved.
// See the LICENSE file located in the main directory for
// the details.

#ifdef CXXTEST_RUNNING
#include <array>
#include <map>

#include "../Game/Vec2D.h"
#endif /*CXXTEST_RUNNING*/

enum buttonID {
    button_A,
    button_B,
    button_X,
    button_Y,

    button_Up,
    button_Down,
    button_Left,
    button_Right,

    button_Select,
    button_Start,

    button_L1,
    button_R1,
    button_L3,
    button_R3
};

enum stickID {
    stick_L,  // sticks[] x,y 0-1
    stick_R,  // aticks[] x,y 2-3
    stick_L2, // aticks[] x   4
    stick_R2  // aticks[] x   5
};

/*ControlDevice Class
-------------------
 - Mantains a register of buttons active as well as any joysticks present.
 - Can reset the state of the buttons and joysticks.
 - Derived classes should map their input to these buttons*/
class ControlDevice {
public:
    ControlDevice();
    virtual ~ControlDevice();

    virtual void reset();
    virtual void mapButtonToScanCode(buttonID button, int code);
    virtual void mapStickToScanCode(stickID stick, int code);

    virtual void updateButton(buttonID button, bool value);
    virtual void updateButton(int scancode, bool value);
    virtual void updateStick(int scancode, int axis, double value);

    virtual bool GetButtonValue(buttonID button) const;
    virtual Vec2D GetStickPos(stickID stick) const;

protected:
    std::array<bool, 14> buttons; // Current button State
    std::array<double, 6> sticks; // Current joystick state

    std::map<buttonID, int> buttonMap; // Rules to map a physical button actual scan code to a buttonID
    std::map<stickID, int> stickMap; // Rules to map a physical stick actual scan code to a stickID

private:
    void useDefaultMapping();
};

