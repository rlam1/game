// Copyright(c) 2016, Rodolfo Lam
// All rights reserved.
// See the LICENSE file located in the main directory for
// the details.

#include "stdafx.h"
#include "ControllerManager.h"

ControllerManager::ControllerManager() {
    deviceList.reserve(2);
    registerDevices();
}

ControllerManager::~ControllerManager() {
}

int ControllerManager::GetNumOfControllers() const {
    return deviceList.size();
}

bool ControllerManager::UpdateDeviceList() {
    registerDevices();

    return true;
}

void ControllerManager::UpdateDeviceKeyboard(int scancode, int value) {
    deviceList[0].get()->updateButton(scancode, value);
}

void ControllerManager::UpdateDeviceDigital(int scancode, int value) {
    if(deviceList.size() > 1)
    {
        for(unsigned int i = 1; i < deviceList.size(); i++)
        {
            deviceList[i].get()->updateButton(scancode, value);
        }
    } else
    {
        /* There is no controller*/
        return; 
    }
}

void ControllerManager::UpdateDeviceAnalog(int scancode, int axis, double value) {
    if(deviceList.size() > 1)
    {
        try
        {
            for(unsigned int i = 1; i < deviceList.size(); i++)
            {
                deviceList.at(i).get()->updateStick(scancode, axis, value);
            }
            return;
        } catch(const std::out_of_range&)
        {
            /*We drop this update because invalid*/
            return;
        }

    } else
    {
        /* There is no controller*/
        return;
    }
}

bool ControllerManager::GetButton(buttonID id) const{
    bool globalButtonValue = deviceList[0].get()->GetButtonValue(id);

    if(deviceList.size() <= 1)
    {
        return globalButtonValue;
    } else
    {
        for(unsigned int i = 1; i < deviceList.size(); i++)
        {
            globalButtonValue |= deviceList[i].get()->GetButtonValue(id);
        }

        return globalButtonValue;
    }
}

Vec2D ControllerManager::GetAxis(stickID id) const {
    // We will only ever read joysticks from first non-keyboard device
    if(deviceList.size() <= 1)
    {
        return Vec2D();
    } else
    {
        return deviceList[1].get()->GetStickPos(id);
    }
}

void ControllerManager::registerDevices() {
    deviceList.clear();

    al_reconfigure_joysticks();
    int devicesAvailable = al_get_num_joysticks();

    addDevice(std::make_unique<KeybControlDevice>()); //First device is always the keyboard.

    //All other available Devices are then registered to the manager.
    for(int i = 0; i < devicesAvailable; i++)
    {
        addDevice(std::make_unique<ControlDevice>());
    }
}

void ControllerManager::addDevice(std::unique_ptr<ControlDevice> device) {
    deviceList.push_back(std::move(device));
}
