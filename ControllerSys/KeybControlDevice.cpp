// Copyright(c) 2016, Rodolfo Lam
// All rights reserved.
// See the LICENSE file located in the main directory for
// the details.

#include "stdafx.h"
#include "KeybControlDevice.h"


KeybControlDevice::KeybControlDevice() {
    reset();
}


KeybControlDevice::~KeybControlDevice() {}

void KeybControlDevice::reset() {
    buttons.fill(false);
    useKeybDefaultMapping();
}

void KeybControlDevice::mapStickToScanCode(stickID stick, int code) {
    /*Keyboard has no sticks*/
    return;
}

void KeybControlDevice::updateStick(int scancode, int axis, double value) {
    /*Keyboard has no sticks*/
    return;
}

Vec2D KeybControlDevice::GetStickPos(stickID stick) const {
    /*Keyboard has no sticks*/
    return Vec2D();
}

void KeybControlDevice::useKeybDefaultMapping() {
    typedef buttonID b;

    buttonMap[b::button_A] = ALLEGRO_KEY_K; // ATTACK/INTERACT?
    buttonMap[b::button_B] = ALLEGRO_KEY_L; // BLOCK?
    buttonMap[b::button_X] = ALLEGRO_KEY_I; // RAPID SWITCH OF LAST WEAPON?
    buttonMap[b::button_Y] = ALLEGRO_KEY_P; // POWERS/MAGIC/SPECIAL MOVE?
    buttonMap[b::button_Up] = ALLEGRO_KEY_W;
    buttonMap[b::button_Down] = ALLEGRO_KEY_S;
    buttonMap[b::button_Left] = ALLEGRO_KEY_A;
    buttonMap[b::button_Right] = ALLEGRO_KEY_D;
    buttonMap[b::button_Select] = ALLEGRO_KEY_SPACE; // INVENTORY?
    buttonMap[b::button_Start] = ALLEGRO_KEY_ESCAPE; // PAUSE
    buttonMap[b::button_L1] = ALLEGRO_KEY_Q; // CYCLE LEFT WEAPONS
    buttonMap[b::button_R1] = ALLEGRO_KEY_E; // CYCLE RIGHT WEAPONS
    buttonMap[b::button_L3] = ALLEGRO_KEY_MAX + 1; // NOT USED
    buttonMap[b::button_R3] = ALLEGRO_KEY_MAX + 1; // NOT USED
}
