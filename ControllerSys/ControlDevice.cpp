// Copyright(c) 2016, Rodolfo Lam
// All rights reserved.
// See the LICENSE file located in the main directory for
// the details.

#include "stdafx.h"
#include "ControlDevice.h"

ControlDevice::ControlDevice() {
    reset();
}

ControlDevice::~ControlDevice() {}

void ControlDevice::reset() {
    buttons.fill(false);
    sticks.fill(0.0);
    useDefaultMapping();
}

void ControlDevice::mapButtonToScanCode(buttonID button, int code) {
    buttonMap[button] = code;
}

void ControlDevice::mapStickToScanCode(stickID stick, int code) {
    stickMap[stick] = code;
}

void ControlDevice::updateButton(buttonID button, bool value) {
    try
    {
        buttons.at(button) = value;
        return;
    } catch(const std::out_of_range&)
    {
        /*We drop this update because invalid*/
        return;
    }
}

void ControlDevice::updateButton(int scancode, bool value) {
    try
    {
        for(const auto &internalButton : buttonMap)
        {
            if(internalButton.second == scancode)
            {
                buttons.at(internalButton.first) = value;
                return;
            }
        }
    } catch(const std::out_of_range&)
    {
        /*We drop this update because invalid*/
        return;
    }
}

void ControlDevice::updateStick(int scancode, int axis, double value) {
    assert((axis >= 0) && (axis <= 3));

    for(const auto &internalStick : stickMap)
    {
        if(internalStick.second == scancode)
        {
            stickID stick = internalStick.first;
            switch(stick)
            {
                case stick_L:
                    sticks[axis] = value;
                    break;
                case stick_R:
                    sticks[2 + axis] = value;
                    break;
                case stick_L2:
                    sticks[4] = value;
                    break;
                case stick_R2:
                    sticks[5] = value;
                    break;
                default:
                    /* No action*/
                    break;
            }
            return;
        }
    }
}

bool ControlDevice::GetButtonValue(buttonID button) const {
    try
    {
        return buttons.at(button);
    } catch(const std::out_of_range&)
    {
        return false;
    }
}

Vec2D ControlDevice::GetStickPos(stickID stick) const {
    switch(stick)
    {
        case stick_L:
            return Vec2D(sticks[0], sticks[1]);
            break;
        case stick_R:
            return Vec2D(sticks[2], sticks[3]);
            break;
        case stick_L2:
            return Vec2D(sticks[4]);
            break;
        case stick_R2:
            return Vec2D(sticks[5]);
            break;
        default:
            return Vec2D(); // Unknown stick access.
            break;
    }
}

void ControlDevice::useDefaultMapping() {
    typedef buttonID b;
    typedef stickID s;

    buttonMap[b::button_A] = 0;
    buttonMap[b::button_B] = 1;
    buttonMap[b::button_X] = 2;
    buttonMap[b::button_Y] = 3;
    buttonMap[b::button_Up] = 13;
    buttonMap[b::button_Down] = 12;
    buttonMap[b::button_Left] = 11;
    buttonMap[b::button_Right] = 10;
    buttonMap[b::button_Select] = 8;
    buttonMap[b::button_Start] = 9;
    buttonMap[b::button_L1] = 5;
    buttonMap[b::button_R1] = 4;
    buttonMap[b::button_L3] = 7;
    buttonMap[b::button_R3] = 6;

    stickMap[s::stick_L] = 0;
    stickMap[s::stick_R] = 1;
    stickMap[s::stick_L2] = 2;
    stickMap[s::stick_R2] = 3;
}
