#pragma once
// Copyright(c) 2016, Rodolfo Lam
// All rights reserved.
// See the LICENSE file located in the main directory for
// the details.

#include "ControlDevice.h"

/*KeybControlDevice Class
-------------------
- Mantains a register of buttons active.
- Can reset the state of the buttons.
- This class maps the keyboard input to standard buttons,
  as defined on base ControlDevice.*/
class KeybControlDevice :
    public ControlDevice {
public:
    KeybControlDevice();
    virtual ~KeybControlDevice();

    virtual void reset() override;

    virtual void mapStickToScanCode(stickID stick, int code) override;

    virtual void updateStick(int scancode, int axis, double value) override;

    virtual Vec2D GetStickPos(stickID stick) const override;

private:
    void useKeybDefaultMapping();
};

