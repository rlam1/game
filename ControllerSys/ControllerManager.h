#pragma once
// Copyright(c) 2016, Rodolfo Lam
// All rights reserved.
// See the LICENSE file located in the main directory for
// the details.
#ifdef CXXTEST_RUNNING
#include <memory>
#endif /*CXXTEST_RUNNING*/

#include "ControlDevice.h"
#include "KeybControlDevice.h"

/*
ControllerManager Class
-----------------------
- Keeps track of how many controllers are currently connected.
- Handles controller connection, disconnect.
- Owns several instances of actual controllers (ControlDevice).
*/
class ControllerManager {
public:
    ControllerManager();
    virtual ~ControllerManager();

    int GetNumOfControllers() const;

    bool UpdateDeviceList(); /*NO TEST*/
    void UpdateDeviceKeyboard(int scancode, int value);
    void UpdateDeviceDigital(int scancode, int value); /*NO TEST*/
    void UpdateDeviceAnalog(int scancode, int axis, double value); /*NO TEST*/

    bool GetButton(buttonID id) const;
    Vec2D GetAxis(stickID id) const;

private:
    std::vector<std::unique_ptr<ControlDevice>> deviceList;

    void registerDevices();
    void addDevice(std::unique_ptr<ControlDevice> device);
};

